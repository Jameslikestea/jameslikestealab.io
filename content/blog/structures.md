---
title: "Structures"
date: 2020-10-26T21:03:58Z
slug: ""
description: ""
keywords: []
draft: false
tags:
  - structures
  - go
  - language
math: false
toc: false
---

As a language, JSON has some basic building blocks, we call these primitives. Aside from primitives, JSON also has a couple of other types, Objects and Arrays, these behave like maps and lists in Python or maps and slices in Go.

## The Primatives

The first thing that we're going to look at are the primitives, below is a list of types and examples of what those primitives look like.

| Type        | Example         |
| ----------- | --------------- |
| **string**  | `"Hello World"` |
| **number**  | `101.203`       |
| **boolean** | `true`          |
| **null**    | `null`          |

These types are the lowest form that JSON accepts. They represent small values that can be used in the compound types to create larger data structures.

## The compounds

The next data types that we need to look at are compound data types, these are a way of structuring the primitives to create those larger data structures that we've come to know. Below is a list of these data types

| Type       | Example                         |
| ---------- | ------------------------------- |
| **object** | `{"hello": "world"}`            |
| **array**  | `["hello", {"hello": "world"}]` |

There are fewer compound data types than there are of the primitives, however this is all that's needed for JSON.

JSON is easier to parse than something like YAML as it is not whitespace delimited or indent-sensitive. This means that you can write a single line for JSON or multi line, it just doesn't matter.
