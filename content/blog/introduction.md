---
title: "Introduction"
date: 2020-10-26T20:44:45Z
slug: ""
description: ""
keywords: []
draft: false
tags:
  - go
math: false
toc: true
---

Rather than building our own language it is probably better that we build something that is for an existing language. As such we're going to build a JSON parser and accessor in Go, the parser that we'll build will be slightly more developer friendly than the standard `map[interface]interface{}{}` that Go normally provides.
